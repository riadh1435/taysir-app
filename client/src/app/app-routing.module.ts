import { TashihTilawaFatihaComponent } from './tashih-tilawa-fatiha/tashih-tilawa-fatiha.component';
import { TashihTilawaComponent } from './tashih-tilawa/tashih-tilawa.component';
import { TashihKiraaKalounComponent } from './tashih-kiraa-kaloun/tashih-kiraa-kaloun.component';

/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {NgModule,Injectable} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaceratounaComponent } from './maceratouna/maceratouna.component';
import {HomeComponent} from './home/home.component';
import {IjazaComponent} from'./ijaza/ijaza.component';
import {KiraaComponent}from './kiraa/kiraa.component';
import {TilawaComponent} from './tilawa/tilawa.component';
import {TashihComponent} from './tashih/tashih.component';
import {HafsComponent} from './hafs/hafs.component';
import { KalounComponent } from './kaloun/kaloun.component';
const routes: Routes = [
  { path: '', component: HomeComponent, },
  { path: 'ijaza', component: IjazaComponent, },
  { path: 'meceratouna', component: MaceratounaComponent },
  { path: 'kiraa', component: KiraaComponent },
  { path: 'tilawa', component: TilawaComponent },
  { path: 'hafs', component: HafsComponent },
  { path: 'kaloun', component: KalounComponent },
  { path: 'tashih', component: TashihComponent },
  {path:'TashihKiraaKaloun',component:TashihKiraaKalounComponent},
  {path :'tashihTilawa', component:TashihTilawaComponent},
  {path :'tashihTilawaFatiha', component:TashihTilawaFatihaComponent}
];
@NgModule({
  imports: 
  [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
  declarations: [],
  providers: []
})


export class AppRoutingModule { }