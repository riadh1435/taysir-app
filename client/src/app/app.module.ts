/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { IoService } from './services/io.service';
import { FulfillmentService} from './services/fulfillment.service';
import { EventService} from './services/event.service';
import { MicrophoneComponent } from './microphone/microphone.component';
import { DialogflowComponent } from './dialogflow/dialogflow.component';
import { WaveformComponent } from './waveform/waveform.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import { NavbarComponent } from './navbar/navbar.component';
import { MaceratounaComponent } from './maceratouna/maceratouna.component';
import {HomeComponent} from './home/home.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import { IjazaComponent } from './ijaza/ijaza.component';
import { TilawaComponent } from './tilawa/tilawa.component';
import { KiraaComponent } from './kiraa/kiraa.component';
import { TashihComponent } from './tashih/tashih.component';
import { HafsComponent } from './hafs/hafs.component';
import { KalounComponent } from './kaloun/kaloun.component';
import { FooterComponent } from './footer/footer.component';
import { TashihKiraaKalounComponent } from './tashih-kiraa-kaloun/tashih-kiraa-kaloun.component';
import { TashihTilawaComponent } from './tashih-tilawa/tashih-tilawa.component';
import { TashihTilawaFatihaComponent } from './tashih-tilawa-fatiha/tashih-tilawa-fatiha.component';

@NgModule({
  declarations: [
    AppComponent,
    MicrophoneComponent,
    DialogflowComponent,
    WaveformComponent,
    NavbarComponent,
    MaceratounaComponent,
    HomeComponent,
    IjazaComponent,
    TilawaComponent,
    KiraaComponent,
    TashihComponent,
    HafsComponent,
    KalounComponent,
    FooterComponent,
    TashihKiraaKalounComponent,
    TashihTilawaComponent,
    TashihTilawaFatihaComponent
      ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatToolbarModule,
    MatCardModule,
    MatChipsModule,
    MatButtonModule,

    MatIconModule,
    MatDialogModule,
    MatSelectModule,
    NgxPageScrollModule
   ],
   exports: [
    MatIconModule
  ],
   providers: [
    IoService,
    FulfillmentService,
    EventService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
