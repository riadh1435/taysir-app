import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HafsComponent } from './hafs.component';

describe('HafsComponent', () => {
  let component: HafsComponent;
  let fixture: ComponentFixture<HafsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HafsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HafsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
