import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IjazaComponent } from './ijaza.component';

describe('IjazaComponent', () => {
  let component: IjazaComponent;
  let fixture: ComponentFixture<IjazaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IjazaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IjazaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
