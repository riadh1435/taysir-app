import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KalounComponent } from './kaloun.component';

describe('KalounComponent', () => {
  let component: KalounComponent;
  let fixture: ComponentFixture<KalounComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KalounComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KalounComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
