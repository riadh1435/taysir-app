import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KiraaComponent } from './kiraa.component';

describe('KiraaComponent', () => {
  let component: KiraaComponent;
  let fixture: ComponentFixture<KiraaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KiraaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KiraaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
