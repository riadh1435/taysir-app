import { DialogflowComponent } from './../dialogflow/dialogflow.component';
import { WaveformComponent } from './../waveform/waveform.component';
import { MicrophoneComponent } from './../microphone/microphone.component';

import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/event.service';
import { IoService } from '../services/io.service';
import {AppRoutingModule} from '../app-routing.module';
import { NavigationEnd, Router } from '@angular/router';
import { PageScrollService } from 'ngx-page-scroll-core';
@Component({
  selector: 'app-kiraa',
  templateUrl: './kiraa.component.html',
  styleUrls: ['./kiraa.component.css']
})
export class KiraaComponent implements OnInit {
  public isInActive: boolean;
  title = 'برنامج تسميع القرآن الكريم';  
  public currentTabIndex = 0;
  public links = [
   
  
    { route: ['/kiraa'],
    name: 'مسار تصحيح القراْة',
      
    },
    {route: ['/tilawa'],
    name: 'مسار تصحيح التلاوة',
       },
       {
        route: ['/ijaza'],
        name: 'همّة حتى القمّة (إجازة)',
      }
      ];
  constructor() { }

  ngOnInit() {
  }

}
