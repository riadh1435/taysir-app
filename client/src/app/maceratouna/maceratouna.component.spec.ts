import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaceratounaComponent } from './maceratouna.component';

describe('MaceratounaComponent', () => {
  let component: MaceratounaComponent;
  let fixture: ComponentFixture<MaceratounaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaceratounaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaceratounaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
