/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */

import { Component, Input } from '@angular/core';
import { IoService } from '../services/io.service';
import { WaveformComponent } from '../waveform/waveform.component';
import { EventService } from '../services/event.service';
import { FulfillmentService } from '../services/fulfillment.service';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconModule} from '@angular/material/icon'

declare const RecordRTC: any;
declare const StereoAudioRecorder: any;
declare const THUMBUP_ICON = '<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"> <path d="M0 0h24v24H0z" fill="none"/> <path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-. 44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-1.91l-.01-.01L23 10z"/> </svg>';
@Component({
  selector: 'app-microphone',
  templateUrl: './microphone.component.html',
  styleUrls: ['./microphone.component.scss']
})

export class MicrophoneComponent {
    @Input() waveform: WaveformComponent;
    public utterance: any;
    public recordAudio: any;
    public startDisabled: boolean;
    public stopDisabled: boolean;
    public textbtn:string;
  public  started:boolean;
  public  stopped:boolean;
 
    constructor(public fulfillmentService: FulfillmentService, public ioService: IoService, public eventService: EventService,iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
      let me = this;
      iconRegistry.addSvgIcon(
        'thumbs-up',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/examples/thumbup-icon.svg'));
      console.log("in construct");
      me.started=false;
      this.stopped=true;
      me.startDisabled = false;
     
      me.eventService.audioPlaying.subscribe(() => {
        me.onStop();
        console.log("me.onSTop in construct audioPlaying service");
      });
      me.eventService.resetInterface.subscribe(() => {
        console.log("reset service");
        me.onStop(); // stop recording & waveform
        me.eventService.audioStopping.emit(); // stop playing audio
        me.reset(); // reset the interface
      });
    }
decln(){
  console.log('in decln');
  let me = this;
  console.log("started or not")
  console.log(me.started);
  if(!me.started ){
    me.started=true;
    me.stopped=false;
    
    this.onStart();

  }
  else{ me.started=false;me.stopped=true;
    
    this.onStop();
  }
}
    onStart() {
      let me = this;
     
        console.log("in start");
        
      // make use of HTML 5/WebRTC, JavaScript getUserMedia()
      // to capture the browser microphone stream
      navigator.mediaDevices.getUserMedia({
          audio: true
      }).then(function(stream: MediaStream) {
          me.recordAudio = RecordRTC(stream, {
              type: 'audio',
              mimeType: 'audio/webm',
              sampleRate: 44100, // this sampleRate should be the same in your server code

              // MediaStreamRecorder, StereoAudioRecorder, WebAssemblyRecorder
              // CanvasRecorder, GifRecorder, WhammyRecorder
              recorderType: StereoAudioRecorder,

              // Dialogflow / STT requires mono audio
              numberOfAudioChannels: 1,

              // get intervals based blobs
              // value in milliseconds
              // as you might not want to make detect calls every seconds
              timeSlice: 5000,

              // only for audio track
              // audioBitsPerSecond: 128000,

              // used by StereoAudioRecorder
              // the range 22050 to 96000.
              // let us force 16khz recording:
              desiredSampRate: 16000,

              // as soon as the stream is available
              ondataavailable(blob) {
                if(!me.eventService.getIsPlaying()) {
                  console.log('in on data available blob');
                  me.ioService.sendBinaryStream(blob);
                  me.waveform.visualize();
                }
              }
          });
          me.recordAudio.startRecording();
          console.log("start recording");
          // recording started

          me.waveform.start(stream);
      }).catch(function(error) {
          console.error(JSON.stringify(error));
      });
    
 }

    onStop() {

      console.log("in stop");
      // recording stopped
    
      console.log("this.startdisabled ")
      // stop audio recorder
    
      this.recordAudio.stopRecording();
      this.waveform.stop();
    }

    reset() {
      this.fulfillmentService.clearAll();
    }
    getUrl()
{
  return "url('../../assets/images/pattern-bg.jpg')";
}
}
