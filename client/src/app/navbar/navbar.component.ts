import {AppRoutingModule} from '../app-routing.module';
import { NavigationEnd, Router } from '@angular/router';
import { PageScrollService } from 'ngx-page-scroll-core';

import { Component, OnInit } from '@angular/core';
declare var foo: Function;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public links = [
    { route: ['/'],
    name: 'الرّئيسيّة',
    },
    { route: ['/meceratouna'],
    name: 'مساراتنا',
    },
    { route: ['/kiraa'],
    name: 'مسار تصحيح القراْة',
    },
    {route: ['/tilawa'],
    name: 'مسار تصحيح التلاوة',
       },
       {
        route: ['/ijaza'],
        name: 'همّة حتى القمّة (إجازة)',
      }
      ];
      public isInActive:boolean;
      public currentTabIndex = 0;

  constructor(private router: Router, private pageScrollService: PageScrollService) {
    this.isInActive = true;
    
    router.events.subscribe((event) => {
      // see also
      if (event instanceof NavigationEnd) {
        this.links.forEach((link, index) => {
          if (router.isActive(router.createUrlTree(link.route), false)) {
            this.currentTabIndex = index;
          }
        });
      }
    });
  }

  ngOnInit() {
  }
  public tabChange(event: any): void {
    // Select the correct route for that tab
    const routeObj = this.links[event.index];
    if (routeObj && routeObj.route) {
      this.router.navigate(routeObj.route);
    }
  }s
}
