/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Fulfillment } from '../models/fulfillment.model';
import {NgModule,Injectable} from '@angular/core';
@Injectable({
    providedIn:'root'
})
@NgModule({
    providers:[]
})
export class FulfillmentService {
    public fulfillment: Fulfillment;
    public matches: any;
    private defaultMessage: string;
public fatihah_hafs: string="الحمد لله رب العالمين الرحمان الرحيم مالك يوم الدين إياك نعبد و إياك نستعين اهدنا الصراط المستقيم صراط الذين أنعمت عليهم غير المغضوب عليهم ولا الضالين";
 public fatihah_tmp:string = "الحمد لله رب العالمين الرحمان الرحيم مالك يوم الدين إياك نعبد و إياك نستعين اهدنا الصراط المستقيم صراط الذين أنعمت عليهم غير المغضوب عليهم ولا الضالين";

constructor() {
        this.defaultMessage = '';
        this.matches = [];
        this.fulfillment = {
            UTTERANCE: this.defaultMessage,
            FULFILLMENTS: [],
            color:"green"
            
        };
    }
    getFulfillment() {
        return this.fulfillment;
    }
    setFulfillments(data) {
        
        
        //console.log(data);
        if (data == null) {
            return;
        }
        if (data.UTTERANCE) {
           // if(){} intervenir ici 
            this.fulfillment.UTTERANCE = data.UTTERANCE;
        } else {
            if (data.PAYLOAD) {
                let payload = JSON.parse(data.PAYLOAD);
                this.matches.push({
                    QUESTION: payload.QUESTION,
                    ANSWER: data.TRANSLATED_FULFILLMENT
                    // ANSWER: payload.ANSWER
                });
            } else {
                this.matches.push({
                    QUESTION: data.INTENT_NAME,
                    ANSWER: data.TRANSLATED_FULFILLMENT,
                    AUDIO: data.AUDIO
                });
            }
            this.fulfillment.FULFILLMENTS = this.matches;
        }
    }
    clearAll() {
        this.matches = [];
        this.fulfillment.UTTERANCE = this.defaultMessage;
        return this.fulfillment.FULFILLMENTS = [];
    }
}
