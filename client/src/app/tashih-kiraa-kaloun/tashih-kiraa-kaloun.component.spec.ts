import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TashihKiraaKalounComponent } from './tashih-kiraa-kaloun.component';

describe('TashihKiraaKalounComponent', () => {
  let component: TashihKiraaKalounComponent;
  let fixture: ComponentFixture<TashihKiraaKalounComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TashihKiraaKalounComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TashihKiraaKalounComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
