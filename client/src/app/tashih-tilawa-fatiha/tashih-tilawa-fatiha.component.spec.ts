import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TashihTilawaFatihaComponent } from './tashih-tilawa-fatiha.component';

describe('TashihTilawaFatihaComponent', () => {
  let component: TashihTilawaFatihaComponent;
  let fixture: ComponentFixture<TashihTilawaFatihaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TashihTilawaFatihaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TashihTilawaFatihaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
