import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TashihTilawaComponent } from './tashih-tilawa.component';

describe('TashihTilawaComponent', () => {
  let component: TashihTilawaComponent;
  let fixture: ComponentFixture<TashihTilawaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TashihTilawaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TashihTilawaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
