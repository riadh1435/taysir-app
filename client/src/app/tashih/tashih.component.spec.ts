import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TashihComponent } from './tashih.component';

describe('TashihComponent', () => {
  let component: TashihComponent;
  let fixture: ComponentFixture<TashihComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TashihComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TashihComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
