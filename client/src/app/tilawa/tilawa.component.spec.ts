import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TilawaComponent } from './tilawa.component';

describe('TilawaComponent', () => {
  let component: TilawaComponent;
  let fixture: ComponentFixture<TilawaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TilawaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TilawaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
